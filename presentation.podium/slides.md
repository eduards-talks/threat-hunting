class: title

# Modern Threat Hunting
## Eduard Thamm

???

Welcome and introduction

The title does not imply it but what we are talking here is "at scale".

_Anyone can protect a single endpoint. It is easy just turn it of and never use it._

---

# Content

- What is threat hunting and why do it?
- What are the approaches you can take?
- Modern Techniques and Tools
  - Osquery
- Questions and Discussion

???

This talk focuses on a narrow subset of the tactics, techniques and procedures

found in modern threat hunting and does not even attempt to dive into the classic

counter intelligence work that is at the core of all of these shenanigans

---

# What?

## "the process of proactively and iteratively searching through systems to detect and isolate threats that evade existing security solutions"

---

# Why?

- To find threats that by definition slip past all other defenses
- To gain insights into structural weaknesses of your system
- Increase observability
- Increase understanding of system
- Improve response process efficiency
- ...
- So your SoC Analyst does not get bored

---
# Approaches

- Passive/Passive (Not really threat hunting now is it.)
- Active/Passive
- Active/Active

???

I have included the passive/passive approach here because it is a model that
although it is explicitly not threat hunting is often found in the industry under
that name.

Passive/Passive: Classical defense mechanism: Firewalls, AV's, IDS's,... they do not allow for prowling the perimeter
Active/Passive: Tools that allow for queries and identification but do not aid in eradication: Osquery, Ossec
Active/Active: Tools that do not only allow for identification, eradication and potentially aid in recovery: GRR

---
# Osquery Basics

- Exposes system information through SQL
- Has an interactive and daemon mode
- Allows for insights into
  - Security
  - Performance
  - Configuration
  - State
- Has a plugin architecture
- Works on Linux, Windows, FreeBSD and MacOs

???
It is based on SQLite but does not have or cache tables.

Works with the concept of virtual tables that are generated on-the-fly.

Plugin architecture mean that you can easily write custom tables/config plugins/loggers.
(C++,Python,Go) -> Thrift API. Caveat: Try avoiding stuff with a "heavy" runtime.

---
# Some Examples - Listeners

Let's say you want to know all processes listening on network ports. You could just:

```sql
SELECT DISTINCT process.name, listening.port, listening.address, process.pid
FROM processes AS process
JOIN listening_ports AS listening
ON process.pid = listening.pid;
```

Now if this list changes over time or contains a well known signature, that might be an IOC.

---
# Some More Examples




```sql
select name, path, pid FROM processes WHERE on_disk = 0;
select name from kernel_modules;
select * from npm_packages;
select * from time;
```

???

Let's look for processes without a binary on disk

or loaded kernel modules

or how about the installed npm packages

or maybe you just want to know how late it is

---
# Query Packs

- Aggregates queries with similar goals (e.g. rootkit detection)
- Author and community maintained
- Write your own
- Mostly used with osqueryd

???

Talk about snapshot vs diff queries/logs

---
# Output

- File (ship to aggregator)
- TLS (Self implemented or FOSS/Commercial Projects)
- Syslog/Event_Log
- Kinesis
- Firehose
- Kafka

---
class: title

# Questions and Discussion

---
class: title

# Thank you
